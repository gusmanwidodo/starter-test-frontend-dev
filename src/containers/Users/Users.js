import React, { Component } from 'react';

const Spinner = () => (
  <div className="container">
    <div className="text-center">
      <div className="spinner-grow" role="status">
        <span className="sr-only">Loading...</span>
      </div>
    </div>
  </div>
);

class Users extends Component {
  constructor() {
    super();
    this.state = {
      users: [],
      isLoading: true
    }
  }

  componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/users')
      .then(results => {
        return results.json();
      }).then(data => {
        let users = data.map((item, index) => {
          console.log(item)
          return (
            <div key={index} className="col-md-4">
              <div className="card mb-4 shadow-sm">
                <div className="card-body">
                  <table>
                    <tbody>
                      <tr>
                        <td>Username</td>
                        <td>: <strong>{item.username}</strong></td>
                      </tr>
                      <tr>
                        <td>Name</td>
                        <td>: {item.name}</td>
                      </tr>
                      <tr>
                        <td>Company</td>
                        <td>: {item.company.name}</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td>: {item.email}</td>
                      </tr>
                      <tr>
                        <td>Address</td>
                        <td>: {item.address.street}, {item.address.suite}, {item.address.city} - {item.address.zipcode} | <a href={`https://www.google.com/maps/?q=${item.address.geo.lat},${item.address.geo.lng}`} target="_blank" rel="noopener noreferrer">open map</a></td>
                      </tr>
                      <tr>
                        <td>Phone</td>
                        <td>: <a href={`tel:${item.phone}`}>{item.phone}</a></td>
                      </tr>
                      <tr>
                        <td>Website</td>
                        <td>: <a href={`http://${item.website}`} target="_blank" rel="noopener noreferrer">{item.website}</a></td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          )          
        });

        this.setState({users: users, isLoading: false});
      })
  }

  render() {
    return ( 
      <>   
        <div className="container">
          <div className="py-5 text-center">
            <h2>Users</h2>
            <p className="lead">The list of users</p>
          </div>        
        </div>

        <div className="album py-5 bg-light">
          <div className="container">

            <div className="row">              
              {this.state.isLoading ? <Spinner /> : this.state.users}
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default Users;